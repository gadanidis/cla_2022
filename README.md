# Linguistic identity construction in restaurant customer service narratives

This repository contains some supplementary materials for my CLA 2022 poster
presentation "Linguistic identity construction in restaurant customer service
narratives":

- **flash_talk:** recordings of the short talk played at the start of the poster session (with and without subtitles)
- **lda.pdf**: figure showing output of the latent Dirichlet allocation model
- **poster.pdf:** pdf copy of the poster
- **presentation_slides.pdf:** slides for a presentation version of the poster, if you want more detail
- **references.bib**: [BibTeX](https://en.wikipedia.org/wiki/BibTeX) file containing some references and related work
